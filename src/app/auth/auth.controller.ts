import {
  Body,
  Controller,
  UseGuards,
  Request,
  Post,
  Get,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from 'src/app/users/users.service';
import { AuthService } from './auth.service';
import { LoginAuthDto } from './dto/login-auth.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  async login(
    @Body() loginDTO: LoginAuthDto,
  ): Promise<{ access_token: string }> {
    const { username, password } = loginDTO;
    const user = await this.usersService.findOneByUsername(username);
    if (!user) {
      throw new UnauthorizedException('El usuario no existe');
    }
    const valid = await this.authService.validateUser(username, password);
    if (!valid) {
      throw new UnauthorizedException('Credeciales inválidas');
    }
    return await this.authService.generateAccessToken(username);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('user')
  async getUser(@Request() req: any): Promise<any> {
    const { id } = req.user;
    return await this.usersService.findOne(id);
  }
}
