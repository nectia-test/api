import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserEntity } from 'src/app/users/entities/user.entity';
import { UsersService } from 'src/app/users/users.service';
import { JWTPayload } from './jwt.payload';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private usersService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: '$2b$10$a3Ekkb4EUYMoTWIqedpMPO', //   secretOrKey: process.env.JWT_KEY,
    });
    console.log('JWT_KEY', process.env.JWT_KEY);
  }

  async validate(payload: JWTPayload): Promise<UserEntity> {
    const user = await this.usersService.findOne(payload.id);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
