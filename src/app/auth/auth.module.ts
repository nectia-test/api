import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt.strategy';

@Module({
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  imports: [
    UsersModule,
    ConfigModule.forRoot(),
    UsersModule,
    JwtModule.register({
      secret: '$2b$10$a3Ekkb4EUYMoTWIqedpMPO',
      signOptions: { expiresIn: '6000s' },
      // secret: process.env.JWT_KEY,
      // signOptions: { expiresIn: `${process.env.JWT_LIFE}s` },
    }),
  ],
  exports:[AuthService],
})
export class AuthModule {}
