import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateVehicleDto } from './dto/create-vehicle.dto';
import { UpdateVehicleDto } from './dto/update-vehicle.dto';
import { VehicleEntity } from './entities/vehicle.entity';

@Injectable()
export class VehiclesService {
  constructor(
    @InjectRepository(VehicleEntity)
    private vehiclesRepository: Repository<VehicleEntity>,
  ) {}

  async create(createVehicleDto: CreateVehicleDto): Promise<VehicleEntity> {
    const vehicleEntity = await this.vehiclesRepository.save<any>(
      createVehicleDto,
    );
    return vehicleEntity;
  }

  async findAll(
    param: { where?: any; order?: any; take?: number } = {},
  ): Promise<VehicleEntity[]> {
    return this.vehiclesRepository.find({
      ...param,
      relations: ['user'],
    });
  }

  async findOne(id: number): Promise<VehicleEntity> {
    const userEntity: VehicleEntity = await this.vehiclesRepository.findOne({
      where: {
        id: id,
      },
    });
    return userEntity;
  }

  async update(
    id: number,
    updateVehicleDto: UpdateVehicleDto,
  ): Promise<VehicleEntity> {
    await this.vehiclesRepository.update(id, { ...updateVehicleDto });
    const vehicleEntity: VehicleEntity = await this.vehiclesRepository.findOne({
      where: { id },
    });
    return vehicleEntity;
  }

  async remove(id: number): Promise<VehicleEntity> {
    const vehicleEntity = await this.findOne(id);
    await this.vehiclesRepository.delete(id);
    return vehicleEntity;
  }
}
