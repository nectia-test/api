import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { VehiclesService } from '../vehicles.service';

@ValidatorConstraint({ async: true })
@Injectable() // this is needed in order to the class be injected into the module
export class IsEnrollmentAlreadyExistConstraint
  implements ValidatorConstraintInterface
{
  constructor(protected readonly vehiclesService: VehiclesService) {}

  async validate(text: string) {
    const vehicles = await this.vehiclesService.findAll({
      where: {
        enrollment: text,
      },
    });
    return !(vehicles.length >= 1);
  }
}

export function IsEnrollmentAlreadyExist(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsEnrollmentAlreadyExistConstraint,
    });
  };
}
