import { IsNotEmpty, IsString, IsNumber, IsOptional } from 'class-validator';
import { IsEnrollmentAlreadyExist } from '../validators/is-enrollment-already-exists';
import { UserIdExist } from 'src/app/users/validators/user-id-exists';

export class CreateVehicleDto {
  @IsNotEmpty()
  @IsNumber()
  @UserIdExist({
    message: 'El usuario $value no existe.',
  })
  userId: number;

  @IsNotEmpty()
  @IsString()
  @IsEnrollmentAlreadyExist({
    message: 'La matrícula $value ya se encuentra registrada.',
  })
  enrollment: string;

  @IsNotEmpty()
  @IsString()
  brand: string;

  @IsNotEmpty()
  @IsString()
  model: string;

  @IsOptional()
  @IsString()
  detail: string;
}
