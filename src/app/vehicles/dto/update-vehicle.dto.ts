import { IsString, IsNumber, IsOptional } from 'class-validator';
import { UserIdExist } from 'src/app/users/validators/user-id-exists';

export class UpdateVehicleDto {
  @IsOptional()
  @IsNumber()
  @UserIdExist({
    message: 'El usuario $value no existe.',
  })
  userId: number;

  @IsOptional()
  @IsString()
  brand: string;

  @IsOptional()
  @IsString()
  model: string;

  @IsOptional()
  @IsString()
  detail: string;
}
