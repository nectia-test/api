import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { UserEntity } from 'src/app/users/entities/user.entity';

@Entity({
  name: 'vehicles',
})
export class VehicleEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column({
    nullable: false,
  })
  enrollment: string;

  @Column({
    nullable: false,
  })
  brand: string;

  @Column({
    nullable: false,
  })
  model: string;

  @Column({
    nullable: true,
  })
  detail: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.vehicles)
  public user: UserEntity;
}
