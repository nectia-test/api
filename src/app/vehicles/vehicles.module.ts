import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VehicleEntity } from './entities/vehicle.entity';
import { VehiclesService } from './vehicles.service';
import { VehiclesController } from './vehicles.controller';
import { IsEnrollmentAlreadyExistConstraint } from './validators/is-enrollment-already-exists';

const constraints = [IsEnrollmentAlreadyExistConstraint];

@Module({
  controllers: [VehiclesController],
  providers: [VehiclesService, ...constraints],
  imports: [TypeOrmModule.forFeature([VehicleEntity])],
})
export class VehiclesModule {}
