import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { VehiclesModule } from './vehicles/vehicles.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [UsersModule, VehiclesModule, AuthModule],
  exports: [],
})
export class AppModule {}
