import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserEntity> {
    const userEntity = await this.usersRepository.save<any>(createUserDto);

    const salt = '$2b$10$a3Ekkb4EUYMoTWIqedpMPO';
    userEntity.password = await bcrypt.hash(userEntity.password, salt);
    this.usersRepository.save(userEntity);
    return userEntity;
  }

  async findAll(
    param: { where?: any; order?: any; take?: number } = {},
  ): Promise<UserEntity[]> {
    return this.usersRepository.find({
      ...param,
      relations: ['vehicles'],
    });
  }

  async findOne(id: number): Promise<UserEntity> {
    const userEntity: UserEntity = await this.usersRepository.findOne({
      where: {
        id: id,
      },
    });
    return userEntity;
  }
  async findOneByUsername(username: string): Promise<UserEntity> {
    return await this.usersRepository.findOne({
      where: {
        username: username,
      },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<UserEntity> {
    const userEntity: UserEntity = await this.usersRepository.findOne({
      where: { id },
    });
    await this.usersRepository.save({ ...updateUserDto, id: id });
    return userEntity;
  }

  async remove(id: number): Promise<UserEntity> {
    const userEntity = await this.findOne(id);
    await this.usersRepository.delete(id);
    return userEntity;
  }
}
