import * as bcrypt from 'bcrypt';
import {
  BeforeInsert,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import { VehicleEntity } from 'src/app/vehicles/entities/vehicle.entity';
import { Exclude } from 'class-transformer';

@Entity({
  name: 'users',
})
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
  })
  username: string;

  @Column({
    nullable: false,
  })
  @Exclude()
  password: string;

  @OneToMany(() => VehicleEntity, (vehicle: VehicleEntity) => vehicle.user)
  public vehicles: VehicleEntity[];

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compareSync(password, this.password);
  }
}
