import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { IsUsernameAlreadyExistConstraint } from './validators/is-username-already-exists';
import { UserIdExistConstraint } from './validators/user-id-exists';

const constraints = [IsUsernameAlreadyExistConstraint, UserIdExistConstraint];
@Module({
  controllers: [UsersController],
  providers: [UsersService, ...constraints],
  imports: [TypeOrmModule.forFeature([UserEntity])],
  exports: [UsersService, UserIdExistConstraint],
})
export class UsersModule {}
