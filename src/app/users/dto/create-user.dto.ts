import { IsNotEmpty, IsString } from 'class-validator';
import { IsUsernameAlreadyExist } from '../validators/is-username-already-exists';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @IsUsernameAlreadyExist({
    message: 'El nombre de usuario $value ya existe.',
  })
  username: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
